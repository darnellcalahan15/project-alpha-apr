from django.urls import path
from projects.views import task_create_view
from projects.views import my_view

urlpatterns = [
    path("mine/", my_view, name="show_my_tasks"),
    path("create/", task_create_view, name="create_task"),
]
