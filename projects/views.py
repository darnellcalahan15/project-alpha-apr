from django.shortcuts import render, redirect
from .models import Project
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from .forms import Create_Project, Create_task


# Create your views here.
@login_required
def project_view(request):
    all_prjcs = Project.objects.filter(owner=request.user)
    context = {"all_prjcs": all_prjcs}
    return render(request, "projects/main.html", context)


@login_required
def my_view(request):
    my_prjcs = Task.objects.filter(assignee=request.user)
    context = {"my_prjcs": my_prjcs}
    return render(request, "projects/mine.html", context)


@login_required
def project_create_view(request):
    if request.method == "POST":
        form = Create_Project(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")

    else:
        form = Create_Project()

    context = {"form": form}

    return render(request, "projects/create.html", context)


@login_required
def task_create_view(request):
    if request.method == "POST":
        form = Create_task(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")

    else:
        form = Create_task()

    context = {"form": form}

    return render(request, "tasks/create.html", context)
