from django.forms import ModelForm
from .models import Project
from tasks.models import Task


class Create_Project(ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner"]


class Create_task(ModelForm):
    class Meta:
        model = Task
        fields = ["name", "start_date", "due_date", "project", "assignee"]
