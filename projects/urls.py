from django.urls import path
from .views import project_view, project_create_view
from tasks.views import task_view

urlpatterns = [
    path("create/", project_create_view, name="create_project"),
    path("<int:id>/", task_view, name="show_project"),
    path("", project_view, name="list_projects"),
]
